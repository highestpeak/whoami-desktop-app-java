package com.highestpeak.fileutil;

import java.io.IOException;
import java.util.Properties;

/**
 * @author highestpeak
 * todo: 配置文件处理
 */
public class PropertiesUtil {
    public static final String DEFAULT_COMMON_PATH = "";
    private String path;

    private Properties propsFile;

    public PropertiesUtil(String path) throws IOException {
        this.path = path;
        this.propsFile = new Properties();
        propsFile.load(new java.io.FileInputStream(this.path));
    }

    public String read(String key){
        return this.propsFile.getProperty(key);
    }

    public void write(String key,String value){
        this.propsFile.setProperty(key,value);
    }
}
