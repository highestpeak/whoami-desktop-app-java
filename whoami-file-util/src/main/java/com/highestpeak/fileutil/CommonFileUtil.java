package com.highestpeak.fileutil;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * @author highestpeak
 */
public class CommonFileUtil {

    public static final String DEFAULT_CONTENT_CHARSET_NAME = "UTF-8";

    public static class FileNotExistException extends Exception{
        FileNotExistException(String message) {
            super(message);
        }
    }

    public static class FileButGetDirException extends Exception{
        FileButGetDirException(String message) {
            super(message);
        }
    }

    /**
     * future: 网络流\本地流 区分
     */
    public static String readContent(String path) throws Exception {
        File file = new File(path);
        if (!file.exists()){
            throw new FileNotExistException(path);
        }
        if (file.isDirectory()){
            throw new FileButGetDirException(path);
        }

        return FileUtils.readFileToString(file, DEFAULT_CONTENT_CHARSET_NAME);
    }

}
