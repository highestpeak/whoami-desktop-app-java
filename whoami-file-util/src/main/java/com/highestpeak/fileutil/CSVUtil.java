package com.highestpeak.fileutil;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Objects;

/**
 * @author highestpeak
 * todo 读写CSV，作为导出选项？还是本地数据库候选?
 */
public class CSVUtil {
    private String path;
    private List<String> headers;

    private CSVFormat csvFormat;

    public CSVUtil(String path) throws IOException {
        this.path = path;
        this.csvFormat = CSVFormat.DEFAULT;
    }

    public CSVUtil(String path, List<String> headers) throws IOException {
        this(path);

        this.headers = headers;
        Objects.requireNonNull(this.csvFormat);
        this.csvFormat = this.csvFormat.withHeader(headers.toArray(new String[0]));
    }

    public void write(){

    }

    public void delete(){

    }

    public void read(){

    }

    public void update(){

    }

    private void updateFileNow(){

    }
}
