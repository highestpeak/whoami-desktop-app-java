package com.highestpeak.fileutil;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author highestpeak
 */
public class CommonDirUtil {
    public interface PathFilter {
        boolean isMatch(String path);
    }

    public static List<Path> recursiveSearch(String path, PathFilter pathFilter) throws IOException {
        return getFileList(Files.walk(FileSystems.getDefault().getPath(path)),pathFilter);
    }

    public static List<Path> currDirSearch(String path, PathFilter pathFilter) throws IOException {
        return getFileList(Files.list(FileSystems.getDefault().getPath(path)),pathFilter);
    }

    private static List<Path> getFileList(Stream<Path> pathStream, PathFilter pathFilter) {
        return pathStream.filter(path -> pathFilter.isMatch(path.toString()))
                .sorted()
                .collect(Collectors.toList());
    }
}
