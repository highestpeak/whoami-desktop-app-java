package com.highestpeak.markdown;

import com.highestpeak.fileutil.CommonFileUtil;
import com.vladsch.flexmark.ast.Heading;
import com.vladsch.flexmark.ast.Text;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.ast.NodeVisitor;
import com.vladsch.flexmark.util.ast.VisitHandler;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author highestpeak
 */
public class Markdown {

    public static String read(String path) throws Exception {
        return CommonFileUtil.readContent(path);
    }

    public interface HandleMdHeadChain {
        String headChainStr();
    }

    public interface HandleMdText {
        /**
         * 内容处理回调
         *
         * @param text 未转义字符串
         * @param headChainCallback
         */
        void visit(String text, HandleMdHeadChain headChainCallback);
    }

    public static void visitMd(String path, HandleMdText mdTextHandler) throws Exception {
        Parser parser = Parser.builder().build();
        Node document = parser.parse(read(path));
        NodeVisitor visitor = new NodeVisitor();
        Deque<Heading> headingStack = new ArrayDeque<>();
        HandleMdHeadChain headChainCallback = () -> {
            StringBuilder builder = new StringBuilder();
            headingStack.forEach(heading -> builder.insert(0,"->").insert(0,heading.getText()));
            return builder.toString();
        };
        visitor.addHandler(
                new VisitHandler<>(Text.class, textNode -> {
                    mdTextHandler.visit(textNode.getChars().unescape(),headChainCallback);
                    visitor.visitChildren(textNode);
                })
        );
        visitor.addHandler(
                new VisitHandler<>(Heading.class,headingNode -> {
                    updateHeadingStack(headingStack,headingNode);
                    visitor.visitChildren(headingNode);
                })
        );
        visitor.visit(document);
    }

    private static void updateHeadingStack(Deque<Heading> headingStack, Node node) {
        if (!(node instanceof Heading)) {
            return;
        }
        Heading curr = (Heading) node;

        Heading lastHead = null;
        if (headingStack.size() == 0) {
            headingStack.push(curr);
            return;
        }
        lastHead = headingStack.peek();
        if (curr.getLevel() > lastHead.getLevel()){
            headingStack.push(curr);
            return;
        }

        if (curr.getLevel()<lastHead.getLevel()){
            do {
                headingStack.pop();
                lastHead = headingStack.peek();
            } while (lastHead!=null && lastHead.getLevel() != curr.getLevel());
        }
        if (lastHead!=null){
            headingStack.pop();
        }
        headingStack.push(curr);
    }

}
