package com.highestpeak.todo;

import com.highestpeak.todo.entity.DO.TagInfo;

/**
 * @author highestpeak
 * 可以读取数据库中的Tag
 * 可以自定义继承来读取Tag
 */
public abstract class AbstractTagScan {

    /**
     * 标签id
     * 数据库读取实现则返回数据库中Tag的id
     * 自定义继承可以不实现，则返回null
     *
     * @return Tag id
     */
    public String id() {
        return null;
    }

    /**
     * 标签名字
     *
     * @return Tag 名字
     */
    public abstract String name();

    /**
     * 扫描任意内容，例如：一段、一行、整篇内容等等
     *
     * @param text              内容
     * @param afterScanCallback afterScan的任务完成后的回调
     * @return TagInfo 数据库实体类
     */
    public TagInfo scan(String text, AfterScanCallback afterScanCallback) {
        prepare(text);

        if (!test(text)) {
            return null;
        }

        String content = intercept(text);

        if (doAfterScan()) {
            afterScan(afterScanCallback);
        }

        return new TagInfo()
                .setId(id())
                .setTagName(name())
                .setContent(content);
    }

    /**
     * 扫描前的准备工作
     *
     * @param text 待检查文本
     */
    abstract void prepare(String text);

    /**
     * 检查是否匹配当前 Tag
     *
     * @param text 待检查文本
     * @return 是否匹配
     */
    abstract boolean test(String text);

    /**
     * 截取匹配的文本
     *
     * @param text 待截取文本
     * @return 截取的文本
     */
    abstract String intercept(String text);

    /**
     * 是否在匹配之后做相关处理
     *
     * @return 是否处理
     */
    abstract boolean doAfterScan();

    /**
     * afterScan的任务完成后的回调
     */
    public interface AfterScanCallback {
        /**
         * afterScan的任务完成后的回调
         *
         * @param newText 新的字符串
         */
        void invoke(String newText);
    }

    /**
     * Tag匹配之后所作的任务
     *
     * @param afterScanCallback 完成后回调
     */
    abstract void afterScan(AfterScanCallback afterScanCallback);
}
