package com.highestpeak.todo;

import com.highestpeak.fileutil.CommonDirUtil;
import com.highestpeak.markdown.Markdown;
import com.highestpeak.todo.entity.DO.MdTagInfo;
import com.highestpeak.todo.entity.DO.TagInfo;
import com.highestpeak.todo.entity.TagFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author highestpeak
 */
public class TagProcess {

    /**
     * 1.读取 tag 的配置文件 或者 读取用户输入 或者默认(配置文件)
     * 1.1 包括 tag 如何解析、tag扫描到后应当做什么事情
     * 2.扫描指定文件的、文件列表的 tag
     * 3.返回信息
     */

    public static List defaultTagToScan;

    public static List<MdTagInfo> scanMdTag(String path, List<AbstractTagScan> tagToScan) {
        List<MdTagInfo> resultList = new ArrayList<>();
        try {
            Markdown.visitMd(path, (text, headChainCallback) -> tagToScan.forEach(tag -> {
                // todo:
                AbstractTagScan.AfterScanCallback afterScanCallback = newText -> {};
                TagInfo tagInfo = tag.scan(text, afterScanCallback);
                if (tagInfo != null) {
                    MdTagInfo mdTagInfo = (MdTagInfo) tagInfo;
                    mdTagInfo.setArticle(path);
                    mdTagInfo.setHeadingChain(headChainCallback.headChainStr());
                    resultList.add(mdTagInfo);
                }
            }));
        } catch (Exception e) {
            // exception
            e.printStackTrace();
        }
        return resultList;
    }

    public static List<MdTagInfo> scanMdsTag(List<String> paths, List<AbstractTagScan> tagToScan) {
        return paths.stream().flatMap(path -> scanMdTag(path, tagToScan).stream()).collect(Collectors.toList());
    }

    public static List<MdTagInfo> scanDirMdTag(String dir, List<AbstractTagScan> tagToScan) {
        return scanDirMdTag(dir, tagToScan, false);
    }

    public static List<MdTagInfo> scanDirMdTag(String dir, List<AbstractTagScan> tagToScan, boolean recursive) {
        CommonDirUtil.PathFilter pathFilter = path -> path.endsWith(".md");
        List<Path> paths;
        try {
            paths = recursive ? CommonDirUtil.recursiveSearch(dir, pathFilter) :
                    CommonDirUtil.currDirSearch(dir, pathFilter);
        } catch (IOException e) {
            // exception
            e.printStackTrace();
            return null;
        }
        List<String> pathStr = paths.stream().map(Path::toString).collect(Collectors.toList());
        return scanMdsTag(pathStr, tagToScan);
    }

    public static void main(String[] args) {
        String dirPath = "E:\\_code\\code_github\\whoami\\testdata";
        List<String> testPaths = new ArrayList<String>(){{
            add(dirPath+"\\JAVA并发体系-3-并发容器.md");
            add(dirPath+"\\JAVA并发体系-2-锁机制.md");
            add(dirPath+"\\JAVA并发体系-1-线程和任务.md");
        }};
        List<AbstractTagScan> tagToScan = new ArrayList<AbstractTagScan>(){{
            add(new TagRegxScan(TagFactory.todoTag()));
        }};
//        List<MdTagInfo> mdTagInfos = scanMdTag(dirPath + "\\JAVA并发体系-1-线程和任务.md", new ArrayList<AbstractTag>() {{
//            add(new TodoTag());
//        }});
//        List<MdTagInfo> mdTagInfos = scanMdsTag(testPaths, tagToScan);
//        List<MdTagInfo> mdTagInfos = scanDirMdTag(dirPath, tagToScan);
        List<MdTagInfo> mdTagInfos = scanDirMdTag(dirPath, tagToScan,true);
        assert mdTagInfos != null;
        mdTagInfos.forEach(System.out::println);
    }
}
