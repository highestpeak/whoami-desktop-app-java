package com.highestpeak.todo;

/**
 * @author highestpeak
 * todo的存储实现接口
 * 使用泛型：则Tag和TagInfo以及其他实体类均可使用
 */
public interface TodoDataSource<T> {
    /**
     * 写入实体
     *
     * @param entityToWrite 待写入实体
     * @return 写入是否成功
     */
    boolean write(T entityToWrite);

    /**
     * 删除实体
     *
     * @param entityToDel 待删除实体
     * @return 删除是否成功
     */
    boolean del(T entityToDel);

    /**
     * 读取实体
     *
     * @param readArgs 读取参数
     * @return 读取是否成功
     */
    T read(Object readArgs);

    /**
     * 更新实体
     *
     * @param oldEntity 旧实体
     * @param newEntity 新实体
     * @return 更新是否成功
     */
    boolean update(T oldEntity, T newEntity);
}
