package com.highestpeak.todo;

import com.highestpeak.todo.entity.DO.Tag;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author highestpeak
 * 存储在数据库中的Tag的scan
 * 使用正则表达式的Tag的scan
 */
public class TagRegxScan extends AbstractTagScan {
    private Tag tag;

    public TagRegxScan(Tag tag) {
        this.tag = tag;
    }

    @Override
    public String id() {
        return tag.getId();
    }

    @Override
    public String name() {
        return tag.getName();
    }

    private Matcher tagMatcher;

    @Override
    void prepare(String text) {
        Pattern tagPattern = Pattern.compile(tag.getMatchRegx());
        tagMatcher = tagPattern.matcher(text);
    }

    @Override
    boolean test(String text) {
        return tagMatcher.find();
    }

    @Override
    String intercept(String text) {
        if (!tagMatcher.find()) {
            return text;
        }
        Pattern interceptPattern = Pattern.compile(tag.getInterceptRegx());
        Matcher interceptMatcher = interceptPattern.matcher(text);
        if (!interceptMatcher.find()) {
            return text;
        }
        return interceptMatcher.group();
    }

    /**
     * 从数据库中读取的是用来替换的Regx表达式
     * 替换的是matchRegx的内容
     *
     * @return 是否执行afterScan任务
     */
    @Override
    boolean doAfterScan() {
        return tag.getReplaceRegx() != null;
    }

    @Override
    void afterScan(AfterScanCallback afterScanCallback) {
        afterScanCallback.invoke(tagMatcher.replaceAll(tag.getReplaceRegx()));
    }
}
