package com.highestpeak.todo.entity.DO;

/**
 * @author highestpeak
 */
public class MdTagInfo extends TagInfo {
    private String article;
    private String headingChain;

    public MdTagInfo() {
    }

    public String getArticle() {
        return article;
    }

    public MdTagInfo setArticle(String article) {
        this.article = article;
        return this;
    }

    public String getHeadingChain() {
        return headingChain;
    }

    public MdTagInfo setHeadingChain(String headingChain) {
        this.headingChain = headingChain;
        return this;
    }
}
