package com.highestpeak.todo.entity;

import com.highestpeak.todo.entity.DO.Tag;

/**
 * @author highestpeak
 * 内置Tag工厂
 */
public class TagFactory {
    public static final String TODO_REGX = "\\b(todo)\\b.*";

    public static Tag  todoTag(){
        return new Tag()
                .setName("TODO")
                .setDesc("DEFAULT TODO TAG")
                .setMatchRegx(TODO_REGX)
                .setInterceptRegx(TODO_REGX);
    }
}
