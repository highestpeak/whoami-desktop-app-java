package com.highestpeak.todo.entity.DO;

/**
 * @author highestpeak
 */
public class Tag {
    private String id;
    private String name;
    private String desc;
    private String matchRegx;
    private String interceptRegx;
    private String replaceRegx;

    private String category;

    public Tag() {
    }

    public String getId() {
        return id;
    }

    public Tag setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Tag setName(String name) {
        this.name = name;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public Tag setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public String getMatchRegx() {
        return matchRegx;
    }

    public Tag setMatchRegx(String matchRegx) {
        this.matchRegx = matchRegx;
        return this;
    }

    public String getInterceptRegx() {
        return interceptRegx;
    }

    public Tag setInterceptRegx(String interceptRegx) {
        this.interceptRegx = interceptRegx;
        return this;
    }

    public String getReplaceRegx() {
        return replaceRegx;
    }

    public Tag setReplaceRegx(String replaceRegx) {
        this.replaceRegx = replaceRegx;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public Tag setCategory(String category) {
        this.category = category;
        return this;
    }
}
