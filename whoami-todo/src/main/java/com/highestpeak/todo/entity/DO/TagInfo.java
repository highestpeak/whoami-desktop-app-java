package com.highestpeak.todo.entity.DO;

/**
 * @author highestpeak
 * 扫描到的tag信息实体类
 * 可以继承这个实体类来扩展，该实体类可以按字段存储
 * 但是继承后的类的字段应该存储为json类似的结构
 * 并且要存储json规则:即有几个字段、规则时间等
 */
public class TagInfo {
    private String id;
    private String tagId;
    private String tagName;
    private String content;

    public TagInfo() {
    }

    public String getId() {
        return id;
    }

    public TagInfo setId(String id) {
        this.id = id;
        return this;
    }

    public String getTagId() {
        return tagId;
    }

    public TagInfo setTagId(String tagId) {
        this.tagId = tagId;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TagInfo setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public String getContent() {
        return content;
    }

    public TagInfo setContent(String content) {
        this.content = content;
        return this;
    }
}
