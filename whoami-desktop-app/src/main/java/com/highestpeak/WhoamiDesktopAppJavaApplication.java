package com.highestpeak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhoamiDesktopAppJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhoamiDesktopAppJavaApplication.class, args);
    }

}
